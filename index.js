const http = require("http"); // Importing the 'http' module
const fs = require("fs"); // Importing the 'fs' module
const uuid = require("uuid"); // Importing the 'uuid' module
const jsonData = require("./title.json"); // Importing JSON data from 'title.json'

const server = http.createServer((req, res) => {
  // Creating an HTTP server
  if (req.method === "GET" && req.url === "/html") {
    // Checking if the request method is GET and the URL is '/html'
    try {
      // Beginning of try block
      fs.readFile("index.html", "utf-8", (err, data) => {
        // Reading 'index.html' file asynchronously
        if (err) {
          // If an error occurs during file read
          console.log(err); // Logging the error
          res.writeHead(500, { "Content-Type": "text/plain" }); // Setting response status code and headers
          res.end("Server Error"); // Sending server error message
        } else {
          // If file read is successful
          res.writeHead(200, { "Content-Type": "text/html" }); // Setting response status code and headers

          res.end(data); // Ending the response
        }
      });
    } catch (error) {
      // Catching any errors that occur
      console.log(error); // Logging the error
      res.writeHead(500, { "Content-Type": "text/plain" }); // Setting response status code and headers
      res.end("Server Error"); // Sending server error message
    }
  } else if (req.method === "GET" && req.url === "/json") {
    // Checking if the request method is GET and the URL is '/json'
    res.writeHead(200, { "Content-Type": "application/json" }); // Setting response status code and headers
    try {
      // Beginning of try block
      res.end(JSON.stringify(jsonData)); // Ending the response
    } catch (error) {
      // Catching any errors that occur
      console.log(error); // Logging the error
      res.writeHead(500, { "Content-Type": "text/plain" }); // Setting response status code and headers
      res.end("Server Error"); // Sending server error message
    }
  } else if (req.method === "GET" && req.url === "/uuid") {
    // Checking if the request method is GET and the URL is '/uuid'
    try {
      // Beginning of try block
      const data = { uuid: uuid.v4() }; // Generating UUID
      res.writeHead(200, { "Content-Type": "application/json" }); // Setting response status code and headers

      res.end(JSON.stringify(data)); // Ending the response
    } catch (error) {
      // Catching any errors that occur
      console.log(error); // Logging the error
      res.writeHead(500, { "Content-Type": "text/plain" }); // Setting response status code and headers
      res.end("Server Error"); // Sending server error message
    }
  } else if (
    req.method === "GET" &&
    req.url.startsWith("/status/") &&
    !isNaN(Number(req.url.split("/")[2]))
  ) {
    // Checking if the request method is GET, the URL starts with '/status/', and the status code is a valid number
    try {
      // Beginning of try block
      const statusCode = Number(req.url.split("/")[2]); // Extracting status code from URL
      res.writeHead(statusCode, { "Content-Type": "text/plain" }); // Setting response status code and headers
      res.end(`Response with status code ${statusCode}`); // Ending the response
    } catch (error) {
      // Catching any errors that occur
      console.log(error); // Logging the error
      res.writeHead(500, { "Content-Type": "text/plain" }); // Setting response status code and headers
      res.end("Server Error"); // Sending server error message
    }
  } else if (
    req.method === "GET" &&
    req.url.startsWith("/delay/") &&
    !isNaN(parseInt(req.url.split("/")[2]))
  ) {
    // Checking if the request method is GET, the URL starts with '/delay/', and the delay time is a valid number
    try {
      // Beginning of try block
      const delayInSeconds = parseInt(req.url.split("/")[2]); // Extracting delay time from URL
      setTimeout(() => {
        // Setting a timeout to respond after the specified delay
        res.writeHead(200, { "Content-Type": "text/plain" }); // Setting response status code and headers
        res.end(`Delayed response after ${delayInSeconds} seconds`); // Writing response message
      }, delayInSeconds * 1000); // Calculating delay time in milliseconds
    } catch (error) {
      // Catching any errors that occur
      console.log(error); // Logging the error
      res.writeHead(500, { "Content-Type": "text/plain" }); // Setting response status code and headers
      res.end("Server Error"); // Sending server error message
    }
  } else {
    // Handling other requests (not '/html', '/json', '/uuid', '/status/{status_code}', or '/delay/{delay_in_seconds}')
    res.writeHead(404, { "Content-Type": "text/plain" }); // Setting response status code and headers
    res.end("404 Not Found"); // Sending 'Not Found' message
  }
});

server.listen(8080, () => {
  // Starting the server and listening on port 8080
  console.log("Server running on port 8080"); // Logging server start message
});
